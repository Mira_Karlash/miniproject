package com.example.mira.miniproject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Mira on 13.02.2016.
 */
public class Game {

    public static int  life = 2;
    public static int count = 0;
    public static final int numberQuestion = 5;
    Context context;

    public String getQuestion() {
        return question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public int getLife1() {
        Log.d("zxcv", Integer.toString(life1));
        return life1;
    }

    public int getLife2() {
        return life2;
    }

    public int getLife3() {
        Log.d("qwer", Integer.toString(life3));
        return life3;
    }

    String question;
    String answer1;
    String answer2;
    String answer3;
    int life1;
    int life2;
    int life3;



    public Game(Context context, String question, String answer1, String answer2, String answer3, int life1, int life2, int life3) {

        this.context = context;
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.life1 = life1;
        this.life2 = life2;
        this.life3 = life3;

    }

    public void onClick(int userAnswer){
        life -= userAnswer;
        if(life <= 0){
            Intent intent = new Intent(context, GameOverActivity.class);
            count = 0;
            life = 2;
            intent.putExtra("zxcv", "Game Over");
            context.startActivity(intent);
        }
        else{
            count++;
            if (count == numberQuestion){
                Intent intent = new Intent(context, GameOverActivity.class);
                count = 0;
                life = 2;
                intent.putExtra("zxcv", "Win");
                context.startActivity(intent);
                Log.d("zxcv", "Win");
            }
        }
    }



}
