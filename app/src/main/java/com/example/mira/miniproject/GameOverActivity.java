package com.example.mira.miniproject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Mira on 13.02.2016.
 */
public class GameOverActivity extends Activity {

    TextView textView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over_activity);

        textView = (TextView) findViewById(R.id.tv_game_over);


        textView.setText(getIntent().getStringExtra("zxcv"));

        button = (Button) findViewById(R.id.try_again);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

}
