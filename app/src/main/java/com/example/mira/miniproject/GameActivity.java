package com.example.mira.miniproject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Mira on 13.02.2016.
 */
public class GameActivity extends Activity {

    TextView question;
    Button answer1, answer2, answer3;
    View.OnClickListener listener;
    ImageView imageView;
    Game[] game = new Game[5];
    Game gameTemp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        question = (TextView) findViewById(R.id.question);
        answer1 = (Button) findViewById(R.id.answer1);
        answer2 = (Button) findViewById(R.id.answer2);
        answer3 = (Button) findViewById(R.id.answer3);

        imageView = (ImageView) findViewById(R.id.game_iv);

        createQuestion();

        answer1.setOnClickListener(listener);
        answer2.setOnClickListener(listener);
        answer3.setOnClickListener(listener);

    }

    private void createQuestion() {
        switch (Game.count) {
            case 0:
                game[0] = new Game(this, getResources().getString(R.string.question), getResources().getString(R.string.answer1)
                        , getResources().getString(R.string.answer2), getResources().getString(R.string.answer3), 1, 2, 0);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.one));
                initView(game[0]);
                break;
            case 1:
                game[1] = new Game(this, getResources().getString(R.string.question1), getResources().getString(R.string.answer11),
                        getResources().getString(R.string.answer12), getResources().getString(R.string.answer13), 0, 2, 1);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.two));
                initView(game[1]);
                break;
            case 2:
                game[2] = new Game(this, getResources().getString(R.string.question2), getResources().getString(R.string.answer21),
                        getResources().getString(R.string.answer22), getResources().getString(R.string.answer23), 2, 1, 0);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.three));
                initView(game[2]);
                break;
            case 3:
                game[3] = new Game(this, getResources().getString(R.string.question3), getResources().getString(R.string.answer31),
                        getResources().getString(R.string.answer32), getResources().getString(R.string.answer33), 1, 0, 2);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.four));
                initView(game[3]);
                break;
            case 4:
                game[4] = new Game(this, getResources().getString(R.string.question4), getResources().getString(R.string.answer41),
                        getResources().getString(R.string.answer42), getResources().getString(R.string.answer43), 1, 2, 0);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.five));
                initView(game[4]);
                break;

        }

    }

    private void initView(final Game game) {
        this.gameTemp = game;
        question.setText(gameTemp.getQuestion());
        answer1.setText(gameTemp.getAnswer1());
        answer2.setText(gameTemp.getAnswer2());
        answer3.setText(gameTemp.getAnswer3());
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.answer1:
                        gameTemp.onClick(gameTemp.getLife1());
                        createQuestion();
                        break;
                    case R.id.answer2:
                        gameTemp.onClick(gameTemp.getLife2());
                        createQuestion();
                        break;
                    case R.id.answer3:
                        gameTemp.onClick(gameTemp.getLife3());
                        createQuestion();
                        break;
                }


            }
        };
    }


}
